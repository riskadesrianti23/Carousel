/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    AppRegistry,
} from 'react-native';
import Carousel from 'react-native-carousel-view';
import {Container, Content, Button, Footer, Right} from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import ResponsiveImage from 'react-native-responsive-image';

export default class Intro extends Component {
    render() {
        return (
            <Container>
            <Content style={styles.footer}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <View style={styles.container}>
                    <Carousel
                        width={400}
                        height={550}
                        delay={1000000}
                        indicatorAtBottom={true}
                        indicatorColor="red">
                        <View style={styles.contentContainer}>
                           
                        <ResponsiveImage style={{ marginTop: responsiveHeight(15) }} initWidth="250" initHeight="150" resizeMode="contain" source={require('../../assets/images/onboarding-11.png')} />
                            
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Slide 1</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Ini adalah carousel</Text>
               
                        </View>
                        <View style={styles.contentContainer}>
                            <ResponsiveImage style={{ marginTop: responsiveHeight(15) }} initWidth="250" initHeight="150" resizeMode="contain" 
                                source={require('../../assets/images/onboarding-12.png')}
                            />
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Slide 2</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Ini adalah carousel</Text>
                        </View>
                        <View style={styles.contentContainer}>
                            <ResponsiveImage style={{ marginTop: responsiveHeight(15) }} initWidth="250" initHeight="150" resizeMode="contain" 
                                source={require('../../assets/images/onboarding-13.png')}
                            />
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Slide 3</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Ini adalah carousel</Text>
                        </View>
                    </Carousel>
                </View>
            </View>
            </Content>
            <Footer style={styles.footer}>
            <Right>
            <Button style={{marginRight:"5%"}} transparent
                            onPress={() => this.props.navigation.navigate("Login")}
                        >
                            <Text uppercase={false} style={styles.baseText}>Lanjut</Text>
                        </Button>
                        </Right>
                        </Footer>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Roboto-Bold',
        color:'black'

    },
    titleText: {
        fontFamily: 'Roboto',
        fontSize: 30,
        color:'black',
        fontWeight: 'bold',
    },
    container: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        backgroundColor: 'white'
    },
});


AppRegistry.registerComponent('Intro', () => Intro);